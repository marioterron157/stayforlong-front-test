Star Wars Taller Characters
===

Ficheros entregados para la prueba: index.html + styles.css.

Aplicación
---

Crear una aplicación parecida a Momentum Dash (https://momentumdash.com/) mostrando la imagen astronómica del día desde la API de la NASA (https://api.nasa.gov/api.html), junto con datos de un personaje al azar de Star Wars que mida más de 170cm (https://swapi.co/).

Toda librería externa que se instale se hará con npm, dejando las dependencias de desarrollo listas para que qualquier usuario pueda probar el projecto ejecutando únicamente 'npm install'.

Puede utilizarse jQuery para realizar las peticiones Ajax, y controlar con promesas y callbacks que cuando se hayan recibido los datos, se muestren en los elementos adecuados; pero es decisión tuya cómo hacerlo.

El desarrollo de la aplicación deberá ser realizado con módulos de JS.

Cada una de las APIs utilizadas irán en su propio módulo, que tendrá un archivo de configuración (json o js). La petición Ajax deberá ir en un módulo externo común a los dos módulos de API.

Cada uno de los módulos se encargará de todo lo relativo a recibir los datos y mostrarlos en la página, pero dónde se muestren dependerá del fichero principal de la aplicación, que será el que incluya la lógica de controlador.

Se deben controlar en todo momento los posibles errores. Por ejemplo: que no se reciban datos en las peticiones, que los elementos DOM no existan...

**NASA APOD**

La API de la NASA permite un uso libre registrándose para recibir una API KEY inmediatamente.

La imagen debe cargarse en el elemento '.astro__img' del html. La información adicional sobre la imagen (title y explanation en el json de respuesta) deberá cargarse en el elemento '.astro__info'.

Se proporcionan los estilos básicos y el html para no tener que preocuparse de ello. Se pueden hacer cambios en estos ficheros para modificar el diseño, pero no es necesario.

El elemento '.toggle' debe funcionar como un botón que muestra y oculta la información de 'title' y 'explanation' sacada de la API de la NASA. Cuando se muestre la información, el elemento '.toggle' deberá contener la clase css '.open' y su texto deberá cambiar de 'i' a 'x' (o cualquier otra forma que indique que el elemento está abierto y haciendo click de nuevo se puede volver a cerrar); volverá a su estado original (sin la clase y con el texto 'i') al volver a hacer click y ocultar la información.

**SWAPI**

Los datos traídos de la API deberán cargarse en el elemento '.sw__character’. Los datos mostrados serán: nombre, nombre de la especie a la que pertenece, estatura y peso. Eres libre de mostrar los datos en el formato que decidas.