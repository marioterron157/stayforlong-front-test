import moment from 'moment';
import { getBackgroundImg, getInfo } from '../services/nasa';
import getPeople from '../services/swapi';

const setPeople = () => {
  // TODO: Implement the method that displays a random character from SWAPI (I haven't had time)
};

const astro = () => {
  const displayTime = () => {
    const [elClock] = document.getElementsByClassName('astro__clock');
    elClock.innerText = moment().format('LT');
  };

  const handleToggle = () => {
    const [elToggle] = document.getElementsByClassName('toggle');
    const [elInfo] = document.getElementsByClassName('astro__info');
    elToggle.onclick = e => {
      e.target.classList.toggle('open');
      e.target.classList.length > 1 ? (e.target.innerText = 'x') : (e.target.innerText = 'i');
      elInfo.classList.toggle('is-active');
    };
  };

  async function setBackground() {
    const imageUrl = await getBackgroundImg();
    const [elBackground] = document.getElementsByClassName('astro__img');

    elBackground.style.backgroundImage = `url(${imageUrl})`;
  }

  async function setBoxInfo() {
    const info = await getInfo();
    const [elInfo] = Array.from(document.getElementsByClassName('astro__info'));

    const [elH2] = Array.from(elInfo.children).filter(el => el.nodeName === 'H2');
    const [elP] = Array.from(elInfo.children).filter(el => el.nodeName === 'P');

    elH2.innerText = info.title;
    elP.innerText = info.explanation;
  }

  const render = () => {
    const INTERVAL_TIME = 1000;

    displayTime();
    setBackground();
    setBoxInfo();
    handleToggle();

    setInterval(() => {
      displayTime();
    }, INTERVAL_TIME);
  };

  render();
};

export default astro;
