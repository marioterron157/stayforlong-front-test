import axios from 'axios';
import config from '../config';

const API = `${config.apis.nasa}?api_key=${config.apiKeys.nasa}`;

const getBackgroundImg = () =>
  axios
    .get(API)
    .then(res => res.data.url)
    .catch(err => err);

const getInfo = () =>
  axios
    .get(API)
    .then(res => res.data)
    .then(data => ({
      explanation: data.explanation,
      title: data.title,
    }))
    .catch(err => err);

export { getBackgroundImg, getInfo };
