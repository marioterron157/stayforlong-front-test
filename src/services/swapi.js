import axios from 'axios';
import config from '../config';

const API = `${config.apis.swapi}/${Math.random() * (89 - 1) + 1}`;

const getCharacter = () =>
  axios
    .get(API)
    .then(res => res.data)
    .catch(err => err);

export default getCharacter;
